# Laravel Proof-Of-Concept (PoC)

The goal of this test is create a simple API with ElasticSearch using Laravel stack.

## Requirements

Build a REST API that works with a resource called "offers" and supports the following operations:

* POST /offers
* GET  /offers
* GET  /offers/:id

As a data store the API must use a SQL database and an Elasticsearch instance for search support. As a main database you are free to choose anyone you are comfortable to work with. 

Before you start to work, fork this repository. You are going to need that in order to send your code for analysis.

## Stack details
* PHP version 7 or higher;
* Laravel (latest version);
* SQL database (at your choice);
* Elasticsearch;

## Application Architecture

You are free to organize your project the way you like to do and use the packages you want as soon as it attends the goals implementing exactly what we are describing on this specification.

Below you are going to find the description of the endpoints of our REST API.

### OFFERS resource

The offers resource is a JSON document because the entire API must support JSON. It contains exactly the same properties as the example below:
```
  {
    "id": "1",
    "title": "15% de desconto na categoria agasalho feminino, confira!",
    "company": "NetShoes",
    "categories": [
      "Roupas e Calçados"
    ],
    "description": "Aproveite o superdesconto na Netshoes e confira lindos agasalhos para você se esquentar nesse inverno!"
    "instructions": "Acesse o site, escolha seus produtos e no carrinho de compras digite seu código no campo 'Vales'.",
    "tags": [
      "agasalho",
      "moda feminina",
      "moda",
      "roupa",
      "roupas",
      "diadora",
      "olympikus",
      "Netshoes"
    ],
    "active": true,
  }
```

When creating documents you may have missing information, but you can't have properties that don't respect the schema above.

### POST /offers

Creates an Offer. When post data to this API the application the expectation is:

* application is going to validate the body payload and reply with errors when they do not respect the data schema;
* the data sent to the endpoint is going to be inserted into a SQL database;
* the api returns HTTP 200 if having success;
* the api returns HTTP 400 if payload doesn't match with the schema;
* the api returns HTTP 500 in case of unexpected error.

### GET /offers

Returns all offers. When sending a GET request to this endpoint:

* the api returns all offers;
* the api returns null if database is empty;
* the api returns HTTP 200 if having success;
* the api returns HTTP 500 in case of unexpected error;

### GET /offers/:id

Returns a single offer by id. When sending a GET request to this endpoint:

* the api returns a single document matching with the id passed on the uri;
* the api returns HTTP 200 if having success;
* the api returns HTTP 404 if no document is found;
* the api returns HTTP 500 in case of unexpected error.

## Things you can't do when building your application
* We don't want additional features. Plese, stay focused on what is being requested;
* If you are not able to implement all features, please implement what you can. Few features with quality are more valuable than all features with failures.
* We want a human readable code. No need to have a full documentation, but some comments and clear coding are good practices;

## Thing we would love to find in your project 
* Some unit or functional test;

## Releasing your PoC
* This task should not take more than 3 days (but in case of issues or taking more time than usual, send that adding your comments about what happened);
* The release is simple. You just need to submit a PULL REQUEST (PR) to this repo with your changes and add a comment telling us you are done;
* Some comments may be added to your PR. Please stay tuned to reply them as needed.

## FAQ

In case of any question just send an e-mail to: atendimento@lik3.com.br